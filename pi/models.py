from django.db import models
from django.contrib.auth.models import User

STATUS = ((0, "Off"),
          (1, "On"))


class Switch(models.Model):
    name = models.CharField(max_length=200, unique=True)
    updated_on = models.DateTimeField(auto_now=True)
    status = models.IntegerField(choices=STATUS, default=0)

    class Meta:
        ordering = ["-updated_on"]

    def __str__(self):
        return self.name
