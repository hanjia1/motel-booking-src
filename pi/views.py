from django.http import JsonResponse
from rest_framework import generics, status
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser

from .models import Switch
from .serializers import SwitchSerializer


class SwitchList(generics.ListCreateAPIView):
    queryset = Switch.objects.all()
    serializer_class = SwitchSerializer


@api_view(['PATCH'])
def switch_update_endpoint(request):
    if request.method == 'PATCH':
        request_data = JSONParser().parse(request)
        pi_switch = Switch.objects.get(name=request_data['name'])
        switch_serializer = SwitchSerializer(pi_switch, data=request_data)
        if switch_serializer.is_valid():
            try:
                switch_serializer.save()
                return JsonResponse(switch_serializer.data, status=status.HTTP_202_ACCEPTED)
            except Exception:
                return JsonResponse(switch_serializer.errors, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return JsonResponse(switch_serializer.errors, status=status.HTTP_400_BAD_REQUEST)