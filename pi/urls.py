from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from pi import views

urlpatterns = [
    path('pi-switch/', views.SwitchList.as_view()),
    path('api/pi-switch/update', views.switch_update_endpoint)
]

urlpatterns = format_suffix_patterns(urlpatterns)
