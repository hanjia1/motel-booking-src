from . import views
from django.urls import include

from django.urls import path
from .feeds import LatestPostsFeed, AtomSiteNewsFeed

urlpatterns = [
    path("feed/rss", LatestPostsFeed(), name="post_feed"),
    path("feed/atom", AtomSiteNewsFeed()),
    path("", views.PostList.as_view(), name="home"),
    path('all/', views.AllPosts.as_view()),
    path('create/', views.post_create, name="post_create_page"),
    path('edit/<int:pk>/', views.post_edit, name="post_edit_page"),
    path("<slug:slug>/", views.post_detail, name="post_detail"),
    path('api/post/create', views.post_create_endpoint)
]