from django.http import JsonResponse
from django.views import generic
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser
from rest_framework import status
from .models import Post
from .forms import CommentForm, PostForm
from django.shortcuts import render, get_object_or_404
from .serializers import PostSerializer
from django.forms import modelformset_factory


class PostList(generic.ListView):
    queryset = Post.objects.filter(status=1).order_by("-created_on")
    template_name = "index.html"
    paginate_by = 3


class AllPosts(generics.ListCreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer


def post_detail(request, slug):
    template_name = "post_detail.html"
    post = get_object_or_404(Post, slug=slug)
    comments = post.comments.filter(active=True).order_by("-created_on")
    new_comment = None
    # Comment posted
    if request.method == "POST":
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid():
            # Create Comment object but don't save to database yet
            new_comment = comment_form.save(commit=False)
            # Assign the current post to the comment
            new_comment.post = post
            # Save the comment to the database
            new_comment.save()
    else:
        comment_form = CommentForm()

    return render(
        request,
        template_name,
        {
            "post": post,
            "comments": comments,
            "new_comment": new_comment,
            "comment_form": comment_form,
        },
    )


def post_create(request):
    template_name = "post_create.html"
    post_form_set = modelformset_factory(Post, fields=('title', 'slug', 'author', 'content', 'status'))
    if request.method == "POST":
        formset = post_form_set(request.POST, request.FILES, queryset=Post.objects.filter(title__startswith='O'))
        for form in formset.forms:
            form.empty_permitted = False
        is_form_valid = formset.is_valid()
        if is_form_valid:
            formset.save()
    else:
        formset = post_form_set(queryset=Post.objects.filter(title__startswith='O'))
    return render(
        request,
        template_name,
        {'formset': formset}
    )


def post_edit(request, pk):
    template_name = "post_edit.html"
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        post_form = PostForm(data=request.POST)
        if post_form.is_valid():
            updated_post = post_form.save(commit=False)
            updated_post.save()
    else:
        post_form = PostForm(instance=post)

    return render(
        request,
        template_name,
        {'post_form': post_form}
    )


@api_view(['POST'])
def post_create_endpoint(request):
    if request.method == 'POST':
        post_data = JSONParser().parse(request)
        post_serializer = PostSerializer(data=post_data)
        if post_serializer.is_valid():
            try:
                post_serializer.create(validated_data=post_data)
                return JsonResponse(post_serializer.data, status=status.HTTP_201_CREATED)
            except Exception:
                return JsonResponse(post_serializer.errors, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return JsonResponse(post_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
