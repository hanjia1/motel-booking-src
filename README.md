# Django
## This is the learning project of Django with Python

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

✨✨✨✨✨✨✨✨✨✨✨✨
# Django
#### Set up Django
https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/development_environment

#### Create an app
```sh
$ python manage.py startapp xxxx
```
https://docs.djangoproject.com/en/1.8/intro/tutorial01/

#### initial database
```sh
$ python manage.py migrate
```
#### Use model to generate SQL
```sh
$ python manage.py sqlmigrate xxxx 0001_initial
```
#### Query sqlite database
https://djangobook.com/mdj2-models/

#### Set up database and Migration
https://python.plainenglish.io/connect-django-with-database-43f1965565e0

#### Modify database
https://www.techwithtim.net/tutorials/django/sqlite3-database/
run command to migrate database change from app to database
```sh 
$ python manage.py makemigrations blog 
```
```sh
$ python manage.py migrate
```

#### Example project
https://djangocentral.com/building-a-blog-application-with-django/
Access the back office
```sh
http://127.0.0.1:8000/admin/
```
save data to database via Form
https://docs.djangoproject.com/en/3.2/topics/forms/modelforms/

# PyCharm
#### Debug Django in pycharm
https://stackoverflow.com/questions/27269574/how-to-run-debug-server-for-django-project-in-pycharm-community-edition
1. In Run -> Edit Configurations create new configuration
2. [+] / Python
3. Name: runserver
4. Scrip Path: path_to/manage.py
5. Parameters: runserver

# Rest Framework
#### rest api project code
https://learndjango.com/tutorials/official-django-rest-framework-tutorial-beginners

#### example of GET DELETE UPDATE POST
https://bezkoder.com/django-rest-api/

#### allow CORS
https://dzone.com/articles/how-to-fix-django-cors-error

# Deploy to Azure
https://stories.mlh.io/deploying-a-basic-django-app-using-azure-app-services-71ec3b21db08

#### Follow this page to troubleshoot django requirements.txt issue
https://stackoverflow.com/questions/58449933/modulenotfounderror-no-module-named-django-when-trying-to-deploy-django-ser
```sh
$ pip freeze > requirements.txt
```
#### Generage staticfiles folder
```sh
$ python manage.py collectstatic
```
#### Deployment static files
https://www.smashingmagazine.com/2020/06/django-highlights-wrangling-static-assets-media-files-part-4/
http://whitenoise.evans.io/en/stable/
https://stackabuse.com/serving-static-files-in-python-with-django-aws-s3-and-whitenoise/

# 3rd party package
```sh
$ pip install django-summernote
```
```sh
$ pip install django-crispy-forms
```
```sh
$ pip install pygments
```
```sh
$ pip install django-cors-headers
```
